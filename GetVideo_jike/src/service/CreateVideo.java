/**
 * @author:稀饭
 * @time:下午11:59:24
 * @filename:CreateVideo.java
 */
package service;

import util.ConnectionUtil;
import bean.Video;

public class CreateVideo extends Thread {
	private Video video = null;

	public CreateVideo(Video video) {
		this.video = video;
	}

	public void run() {
		ConnectionUtil connectionWork = new ConnectionUtil();
		connectionWork.downLoadFromUrl(video);
	}
}
